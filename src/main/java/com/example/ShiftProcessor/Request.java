package com.example.ShiftProcessor;

import com.moveinsync.ets.models.ShiftType;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class Request {
    Long ms;
    ShiftType shiftType;
    Collection<String> venues;
    String buid;
}
