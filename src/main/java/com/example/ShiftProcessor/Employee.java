package com.example.ShiftProcessor;

import lombok.Getter;

@Getter
public class Employee {

    Integer stwId;
    Integer homeId;
    Integer pickUpId;

    Employee(Integer stwId, Integer homeId, Integer pickUpId){
        this.stwId = stwId;
        this.homeId = homeId;
        this.pickUpId = pickUpId;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "stwId=" + stwId +
                ", homeId=" + homeId +
                ", pickUpId=" + pickUpId +
                '}';
    }
}
