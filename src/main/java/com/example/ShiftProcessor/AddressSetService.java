package com.example.ShiftProcessor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AddressSetService {

    private final DBConnection dbConnection;

    public List<String> getEmployeeGeocodes(List<String> empIds) throws SQLException {
        Connection connection = dbConnection.getDBConnection(ETSConfiguration.ETS_DB_URL,ETSConfiguration.BUID);
        List<Integer> employeeIds = empIds.stream().map(id-> Integer.parseInt(id.split("-")[4])).collect(Collectors.toList());
        PreparedStatement statement = connection.prepareStatement("select stwid,homeid,pickupid from address_set where stwid = ANY (?::integer[]);");

        statement.setArray(0,connection.createArrayOf("stwid",employeeIds.stream().toArray()));
        List<Employee> employees = new ArrayList<>();
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            employees.add(new Employee(rs.getInt("stwid"),rs.getInt("homeid"),rs.getInt("pickupid")));
        }
        employees.forEach(employee->System.out.println(employee.toString()));
        List<String> geoCodes = new ArrayList<>();
        return geoCodes;
    }
}
