package com.example.ShiftProcessor;

import com.moveinsync.ets.models.ShiftType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
@Service
public class EmployeeGeocodeService {

    private final ProcessShiftService processShiftService;

    private final ConfigurationService configurationService;

    public List<String> getGeocodesFromEmployeeIds(List<String> employeeIds) throws SQLException {
        return new ArrayList<>();
    }
}
