package com.example.ShiftProcessor;


import com.moveinsync.ets.models.EmployeeShiftEvent;
import com.moveinsync.ets.models.ShiftType;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/process-shift")
public class ProcessShiftController {

    private final ProcessShiftService processShiftService;

    private final ConfigurationService configurationService;

    @GetMapping(value = "/shiftEvent")
    public List<EmployeeShiftEvent> getShiftEvents(@RequestBody Request request){
        return processShiftService.getEmployeeShiftEvents(request.getMs(), request.getShiftType(), request.getVenues(), request.getBuid());
    }

    @GetMapping
    public String processShift(@RequestBody Request request) throws IOException, SQLException {
        processShiftService.processShift(request.getMs(), request.getShiftType(), request.getVenues(), request.getBuid());
        return "done";
    }


}
