package com.example.ShiftProcessor;

import org.apache.commons.lang.StringUtils;

public class ETSConfiguration {

    public static boolean FEATURE_NODAL_POINT_ENABLED = Boolean.FALSE;
    public static boolean FEATURE_SHUTTLE_ROUTING_ENABLED = Boolean.FALSE;
    public static boolean EMPLOYEE_SPECIAL_NEED_FEATURE_ENABLED = Boolean.FALSE;
    public static boolean SPECIAL_NEED_EMPLOYEE_HOME_PICKUP_DROP_FEATURE_ENABLED = Boolean.FALSE;
    public static String ETS_DB_URL = StringUtils.EMPTY;
    public static String BUID = StringUtils.EMPTY;

    public static void updateETSConfiguration(boolean FEATURE_NODAL_POINT_ENABLED, boolean FEATURE_SHUTTLE_ROUTING_ENABLED,
                                              boolean EMPLOYEE_SPECIAL_NEED_FEATURE_ENABLED, boolean SPECIAL_NEED_EMPLOYEE_HOME_PICKUP_DROP_FEATURE_ENABLED,
                                              String ETS_DB_URL, String BUID){
        ETSConfiguration.FEATURE_NODAL_POINT_ENABLED = FEATURE_NODAL_POINT_ENABLED;
        ETSConfiguration.FEATURE_SHUTTLE_ROUTING_ENABLED = FEATURE_SHUTTLE_ROUTING_ENABLED;
        ETSConfiguration.EMPLOYEE_SPECIAL_NEED_FEATURE_ENABLED = EMPLOYEE_SPECIAL_NEED_FEATURE_ENABLED;
        ETSConfiguration.SPECIAL_NEED_EMPLOYEE_HOME_PICKUP_DROP_FEATURE_ENABLED = SPECIAL_NEED_EMPLOYEE_HOME_PICKUP_DROP_FEATURE_ENABLED;
        ETSConfiguration.ETS_DB_URL = ETS_DB_URL;
        ETSConfiguration.BUID = BUID;
    }
}
