package com.example.ShiftProcessor;

import com.mis.config.ConfigClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class ConfigurationService {

    public static ConfigClient configClient = ConfigClient.initialize("preprod","http://staging4.moveinsync.com:8052/");

    public void initializeConfiguration(String buid) throws IOException {
        configClient.setEtsBuid(buid);
        Map<String,String> config = configClient.getEtsPropertyMap();
        ETSConfiguration.updateETSConfiguration(Boolean.parseBoolean(config.get(Constants.FEATURE_NODAL_POINT_ENABLED)), Boolean.parseBoolean(config.get(Constants.FEATURE_SHUTTLE_ROUTING_ENABLED)),
                Boolean.parseBoolean(config.get(Constants.EMPLOYEE_SPECIAL_NEED_FEATURE_ENABLED)), Boolean.parseBoolean(config.get(Constants.SPECIAL_NEED_EMPLOYEE_HOME_PICKUP_DROP_FEATURE_ENABLED)),
                config.get(Constants.ETS_DB_URL), buid);
    }
}
