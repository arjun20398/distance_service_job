package com.example.ShiftProcessor;

import com.mis.configmodels.Configuration;
import com.mis.pc.utils.GsonUtils;
import com.moveinsync.ets.clients.EmployeeScheduleClient;
import com.moveinsync.ets.clients.EmployeeScheduleClientImpl;
import com.moveinsync.ets.models.EmployeeSchedule;
import com.moveinsync.ets.models.EmployeeShiftEvent;
import com.moveinsync.ets.models.ShiftType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProcessShiftService {

    public static EmployeeScheduleClient employeeScheduleClient = new EmployeeScheduleClientImpl("https://staging.moveinsync.com/ets-schedule","qa-Test2");

    private final ConfigurationService configurationService;

    private final AddressSetService addressSetService;

    public void processShift(Long ms, ShiftType shiftType, Collection<String> venues, String buid) throws IOException, SQLException {
        List<String> empIds = getEmployeeIdsFromShiftEvents(ms,shiftType,venues);
        configurationService.initializeConfiguration(buid);
        List<String> homeGeocodes = addressSetService.getEmployeeGeocodes(empIds);
    }

    public List<EmployeeShiftEvent> getEmployeeShiftEvents(Long ms, ShiftType shiftType, Collection<String> venues, String buid) {
        Date date = new Date();
        date.setTime(ms);
        List<EmployeeShiftEvent> employeeShiftEvents = employeeScheduleClient.getEmployeeShiftEvents(date,date, shiftType, venues);
        System.out.println(employeeShiftEvents.size());
        Collection<String> empIds = employeeShiftEvents.stream().map(EmployeeShiftEvent::getEmployeeId).collect(Collectors.toList());
        System.out.println(empIds.size());
        Collection<EmployeeSchedule> employeeSchedules = employeeScheduleClient.getSchedulesForEmployees(empIds,date,date);
        System.out.println(employeeSchedules.size());
        for(EmployeeSchedule employeeSchedule:employeeSchedules) {
            System.out.println(employeeSchedule.getEmployeeId());
        }
        return employeeShiftEvents;
    }

    public List<String> getEmployeeIdsFromShiftEvents(Long ms,ShiftType shiftType, Collection<String> venues){
        Date date = new Date();
        date.setTime(ms);
        List<EmployeeShiftEvent> employeeShiftEvents = employeeScheduleClient.getEmployeeShiftEvents(date,date, shiftType, venues);
        return employeeShiftEvents.stream().map(EmployeeShiftEvent::getEmployeeId).collect(Collectors.toList());
    }
}
